package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.Message;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {
	public void comment(Message comment, int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().comment(connection, comment, messageId);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	
	public List<Comment> select(String userId, String messageId) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			Integer id = null;
			Integer messageid = null;
			if (!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}
			if(!StringUtils.isEmpty(messageId)){
				messageid = Integer.parseInt(messageId);
			}
			List<Comment> messages = new UserCommentDao().select(connection, id, messageid, LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
